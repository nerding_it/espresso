export class ServerError extends Error {
  constructor(message, status) {
    super(message)
    this.status = status;
  }
}

export class InvalidRouteError extends Error {
  constructor(message) {
    super(message)
  }
}

/*
 * The application class ☕, which provide
 *
 **/
class Espresso {
  constructor() {
    this.corsEnabled = false;
    this.corsHeaders = new Headers();
    this.request;
    this.response;
    this.handlers = new Map();
    this.befores = new Map();
    this.afters = new Map()
    this.schedules = new Map()
  }

  #addCorsHeaders(response, headers) {
    response.headers.set('access-control-allow-origin', headers.get('access-control-allow-origin'))
    response.headers.set('access-control-allow-methods', headers.get('access-control-allow-methods'))
    response.headers.set('access-control-allow-headers', headers.get('access-control-allow-headers'))
  }

  #getHandler() {
    for (const [ route, handler] of this.handlers.get(this.request.method.toLowerCase())) {
      const pattern = new URLPattern({ pathname: route })
      if (pattern.test(this.request.url)) {
        const result = pattern.exec(this.request.url)
        if (result.pathname.groups) {
          this.request.params = new Map()
          Object.keys(result.pathname.groups).forEach((k) => {
            this.request.params.set(k, result.pathname.groups[k])
          })
        }
        const url = new URL(this.request.url);
        this.request.query = url.searchParams;
        return handler;
      }
    }
    throw new InvalidRouteError('No route found')
  }

  /*
   * Add middleware before processing the handler
   * @param { Function } handler The handler
   * @returns
   **/
  before(handler) {
    if (!(handler instanceof Function)) {
      throw new Error('Handler must be function')
    }
    this.befores.set(Symbol(), handler)
    return this;
  }

  /*
   * Add middleware after processing the handler
   * @param { Function } handler The handler
   * @returns
   **/
  after(handler) {
    if (!(handler instanceof Function)) {
      throw new Error('Handler must be function')
    }
    this.afters.set(Symbol(), handler)
    return this;
  }

  /*
   * Register a handler for a http method and path
   * @param { string } method The http method in downcase
   * @param { string } path The path string
   * @param { Function }
   **/
  #register(method, path, handler) {
    if (!(handler instanceof Function)) {
      throw new Error('Handler must be function')
    }
    if (!this.handlers.get(method.toLowerCase())) {
      this.handlers.set(method.toLowerCase(), new Map())
    }
    this.handlers.get(method.toLowerCase()).set(path, handler)
  }

  /*
   * Register a handler for a head method
   * @param { string } path The path string
   * @param { Function } handler The handler function
   **/
  head(path, handler) {
    this.#register('head', path, handler)
    return this;
  }

  /*
   * Register a handler for a get method
   * @param { string } path The path string
   * @param { Function } handler The handler function
   **/
  get(path, handler) {
    this.#register('get', path, handler)
    return this;
  }

  /*
   * Register a handler for a post method
   * @param { string } path The path string
   * @param { Function } handler The handler function
   **/
  post(path, handler) {
    this.#register('post', path, handler)
    return this;
  }

  /*
   * Register a handler for a put method
   * @param { string } path The path string
   * @param { Function } handler The handler function
   **/
  put(path, handler) {
    this.#register('put', path, handler)
    return this;
  }

  /*
   * Register a handler for a patch method
   * @param { string } path The path string
   * @param { Function } handler The handler function
   **/
  patch(path, handler) {
    this.#register('patch', path, handler)
    return this;
  }

  /*
   * Register a handler for a delete method
   * @param { string } path The path string
   * @param { Function } handler The handler function
   **/
  delete(path, handler) {
    this.#register('delete', path, handler)
    return this;
  }

  /*
   * Register a handler for options method
   * FIXME: It'll ignore when cors is enabled, need to think
   * @param { string } path The path string
   * @param { Function } handler The handler function
   **/
  options(path, handler) {
    this.#register('options', path, handler)
    return this;
  }

  /*
   * Register a handler for connect method
   * @param { string } path The path string
   * @param { Function } handler The handler function
   **/
  connect(path, handler) {
    this.#register('connect', path, handler)
    return this;
  }

  /*
   * Register a handler for trace method
   * @param { string } path The path string
   * @param { Function } handler The handler function
   **/
  trace(path, handler) {
    this.#register('trace', path, handler)
    return this;
  }

  /*
   * Enable cors
   * @param { object } The object with origins, methods and headers
   **/
  cors({allowOrigin, allowMethods, allowHeaders}) {
    this.corsHeaders.set('access-control-allow-origin', allowOrigin || '*')
    this.corsHeaders.set('access-control-allow-methods', allowMethods || '*')
    this.corsHeaders.set('access-control-allow-headers', allowHeaders || '*')
    this.corsEnabled = true;
    return this;
  }

  /*
   * Register schedule listener
   * @param { string } exp The cron expression
   * @param { Function } handler The callback
   **/
  schedule(exp, handler) {
    this.schedules.set(exp, handler)
    return this;
  }

  /*
   * Handle the fetch event
   * @param { Request } request the request object
   * @param { Object } env The environment variables (i.e. Text variable, kv bindings etc.)
   * @param { Object } context The context it'll have waitUntil function
   * @param { Response } The response object
   **/
  async #handleFetch(request, env, context) {
    let response, handler;
    if (this.corsEnabled) {
      const { method } = request;
      if (method === 'OPTIONS') {
        response = new Response(null, { status: 204})
        this.#addCorsHeaders(response, this.corsHeaders);
        return response;
      }
    }

    this.request = request.clone()
    try {
      handler = this.#getHandler()
    } catch (error) {
      response = new Response(null, { status: 404 })
      if (this.corsEnabled) {
        this.#addCorsHeaders(response, this.corsHeaders);
      }
      return response;
    }

    for (const [_, handler] of this.befores) {
      try {
        await handler.call(this, this.request, env, context)
      } catch (error) {
        response = new Response(JSON.stringify({error: error.message}), {status: error.status || 400})
      } finally {
        if (response instanceof Response) {
          if (!response.headers.has('content-type')) {
            response.headers.set('Content-Type', 'application/json')
          }
          if (this.corsEnabled) {
            this.#addCorsHeaders(response, this.corsHeaders);
          }
          return response;
        }
      }
    }

    try {
      response = await handler.call(globalThis, this.request, env, context)
    } catch (error) {
      const message = JSON.stringify({error: error.message})
      response = new Response(message, {
        status: 500
      })
    } finally {
      if (response instanceof Response) {
        if (response.status === 500) {
          if (this.corsEnabled) {
            if (!response.headers.has('content-type')) {
              response.headers.set('Content-Type', 'application/json')
            }
            this.#addCorsHeaders(response, this.corsHeaders);
          }
          return response;
        }
        else {
          response = new Response(response.body, {
            ...response
          })
          if (this.corsEnabled) {
            this.#addCorsHeaders(response, this.corsHeaders);
          }
        }
      }
      else {
        if (typeof(response) === 'object') {
          if (!(response instanceof Response)) {
            const headers = response.headers;
            response = new Response(response.body ? JSON.stringify(response.body) : null, { status: response.status || 200 })
            if (headers) {
              Object.keys(headers).forEach((header) => {
                response.headers.set(header, headers[header])
              })
            }
          }
        }
        else {
          response = new Response(response.body)
          if (!response.headers.has('content-type')) {
            response.headers.set('Content-Type', 'text/plain')
          }
          if ('headers' in response) {
            for (const [key, value] of response.headers) {
              response.headers.set(key, value)
            }
          }
        }
      }
    }

    for (const [_, handler] of this.afters) {
      let error = false;
      try {
        handler.call(this, response, env, context)
      } catch (error) {
        error = true;
        response = new Response(JSON.stringify({error: error.message}), {status: error.status || 400})
      } finally {
        if (error) {
          if (this.corsEnabled) {
            this.#addCorsHeaders(response, this.corsHeaders);
          }
          return response;
        }
      }
    }
    if (this.corsEnabled) {
      this.#addCorsHeaders(response, this.corsHeaders);
    }
    return response;
  }

  /*
   * Handle schedule event
   * @param { string } exp The cron expression
   * @param { Object } env The environment variables
   **/
  async #handleSchedule(exp, env) {
    if (this.schedules.has(exp)) {
      const handler = this.schedules.get(exp)
      await handler.call(this, env)
    }
    else if (this.schedules.has('default')) {
      const handler = this.schedules.get('default')
      await handler.call(this, env)
    }
    else {
      return `No handler found for cron expression ${exp}`
    }
  }

  /*
   * Run the application
   **/
  brew() {
    addEventListener('fetch', (event) => {
      const { request } = event;
      const context = {
        waitUntil: event.waitUntil
      }
      const env = {}
      if (globalThis instanceof ServiceWorkerGlobalScope) {
        Object.keys(globalThis).forEach((key) => {
          env[key] = globalThis[key]
        })
      }
      event.respondWith(this.#handleFetch(request, env, context))
    })
    if (this.schedules.size > 0) {
      addEventListener('scheduled', (event) => {
        const env = {}
        if (globalThis instanceof ServiceWorkerGlobalScope) {
          Object.keys(globalThis).forEach((key) => {
            env[key] = globalThis[key]
          })
        }
        event.waitUntil(this.#handleSchedule(event.cron || 'default', env))
      })
    }
  }
}

export default new Espresso();
