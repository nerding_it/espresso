import espresso, { ServerError } from '../';

espresso
  .cors({})
  .before(async (request) => {
    console.debug('Request URL logging middleware')
    console.debug(`Requested URL: ${request.url}`)
  })
  .before(async (request, env) => {
    console.debug('Print environment variable middleware')
    console.debug(`Environment variables ${JSON.stringify(env)}`)
  })
  .after((response) => {
    console.debug('Response status code logging middleware')
    console.debug(`Response status: ${response.status}`)
  })
  .after((response, env, context) => {
    console.debug('Caching middleware')
  })
  .after((response, env, context) => {
    console.debug('Cleanup middleware')
  })
  .head('/', async () => {
    return {
      status: 200,
      body: {message: 'I am head'}
    }     
  })
  .get('/', async () => {
    return {
      status: 200,
      body: {message: 'I am get'}
    }     
  })
  .post('/', async (request) => {
    return {
      status: 200,
      body: { message: 'I am post'}
    }
  })
  .put('/', async (request) => {
    return {
      status: 200,
      body: { message: 'I am put'}
    }
  })
  .patch('/', async (request) => {
    return {
      status: 200,
      body: { message: 'I am patch'}
    }
  })
  .delete('/', async (request) => {
    return {
      status: 200,
      body: { message: 'I am delete'}
    }
  })
  .get('/:name', async (request) => {
    return {
      status: 200,
      body: {message: `Hello ${request.params.get('name')}`}
    }     
  })
  .get('/:name/:country', async (request) => {
    return {
      status: 200,
      body: {message: `Hello ${request.params.get('name')} from ${request.params.get('country')}`}
    }     
  })
  .get('/:name/:country/:state', async (request) => {
    return {
      status: 200,
      headers: {
        'X-Custom-Header': 'Hello world!'
      },
      body: {message: `Hello ${request.params.get('name')} from ${request.params.get('country')} from ${request.params.get('state')} and your query is ${request.query.get('query')}`}
    }     
  })
  .schedule('* * * * *', async (env) => {
    console.log('Every minute')
  })
  .schedule('*/2 * * * *', async (env) => {
    console.log('Every two minute')
  })
  .schedule('default', async (env) => {
    console.log('Default')
  })
  .brew()
